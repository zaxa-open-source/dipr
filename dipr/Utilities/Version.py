
DIPR_VERSION = "1.0.dev12"
DIPR_RELEASE_DATE = "05-21-2019"
DIPR_URL = "https://dipr.dev"


def get_version_string():
    return "Version: " + DIPR_VERSION + " (" + DIPR_RELEASE_DATE + ")"
