
Developer Instructions
======================
Development and building of binaries for dipr requires a correctly configured Anaconda3 environment.  An environment
configuration is provided in the "Environment" directory.  To create a dipr environment, open a conda prompt and:

    conda env create -f environment-common.yaml
    
Then

    conda activate diprenv
    
PyInstaller Building Instructions
=================================
Create and activate the conda environment as specified above.  Change to the "pyinstaller" directory and run:

    pyinstaller --onefile dipr.spec
    
If all goes well, a binary for the current platform should be generated in the "pyinstaller/dist" folder.

Installing/building with setup.py
=================================

The setup.py is setup to stub a 'dipr' command in the path.  Just run:

    > python setup.py install
    > dipr --version
    
To build new distributions:

    > python setup.py build sdist bdist_wheel bdist_egg
    
To upload them to PyPi:

**Testing**

    > twine upload --repository-url https://test.pypi.org/legacy/ dist/*
    
**Release**

    > twine upload dist/*

    