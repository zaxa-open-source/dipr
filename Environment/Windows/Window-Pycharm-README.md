
Pycharm and Windows Development
===============================

'dipr' development and testing is meant to be done in a prescribe Conda environment.  The environment can be restored
following the steps below.

Initial setup of the conda environment
--------------------------------------

If the environment has not previously been created, open a conda prompt
using either the Anaconda UI, or open a command prompt and:

Anaconda3:  

    "%USERPROFILE%\Anaconda3\Scripts\activate.bat" base

Miniconda3:
 
    "%USERPROFILE%\Miniconda3\Scripts\activate.bat" base

Then, in this directory:

    conda env create -f environment-common.yml

Update of the conda environment
-------------------------------

Open a conda prompt for this environment (environment name is n the yml file).

    "%USERPROFILE%\Miniconda3\Scripts\activate.bat"  <env_name>

Then

    conda env update -f environment-common.yml

Activating the environment
--------------------------

Once the environment is created you may activate the diprenv from a command prompt
via the "activate.bat" file which will activate the base Anaconda environment, then
the diprenv environment in order to get all the paths right.

Set PyCharm terminal to use Conda
---------------------------------

File -> Settings -> Tools -> Terminal

Shell Path: 
    
    cmd.exe "/K" <path to dipr git repo>\Environment\activate.bat